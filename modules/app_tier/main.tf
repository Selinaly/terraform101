# Any code related to the app tier
# Public subnet, NACLs, SGs, Instances


# Resource aws_instance ec2

resource "aws_instance" "Webserver" {
  ami = "ami-0635ca8333e27394d"
  instance_type = "t2.micro"
  subnet_id = "subnet-953a58cf"
  associate_public_ip_address = true
  security_groups = ["sg-01f897dc42d10c7de", "sg-091c83d3ecee7301e", aws_security_group.sg-webapp-selina.id ] 
  tags = {
      "Name" = "${var.identify_cohort_name}-web-tf"
  }
}

# Resource aws_security_group

resource "aws_security_group" "sg-webapp-selina" {
  description = "allows port 80 and 443 into instance and port 22 from our ip"
  vpc_id = var.vpc_id
  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      description = "allows port 80"
    
    }

  ingress {
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      description = "allows port 443"
    } 

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["95.147.37.162/32"]
      description = "allows port 22 from our ip" 
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
    tags = {
      "Name" = "SG-${var.identify_cohort_name}-tf-allow-web-access"
    }
}      