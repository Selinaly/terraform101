# Provider
## Define ehere to creaste resources and how to authenticate yourself.
## Options: Get key from vaults, from ENV or leave black and assume AWS role (neeeds to ne set to a machine).


provider "aws" {
    region = "eu-west-1"
    # access_key = "my-access-key"
    # secret_key = "my-secret-key"
}

# Things to do with VPC creaction should be in here

module "app_tier" {
  source = "./modules/app_tier"
  # pass some variables 
  vpc_id = var.vpc_id
  identify_cohort_name = var.identify_cohort_name
}
    
# Resources 
## Rescources map to logical or physical things in a Cloud provider.
## For example a ec2 (physical VM) instance of a SG (Logical SG) 
## Each provvider has its library rescources. Use the Documentation
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs

