# Terraform 101 :taco:

**Introduction**

Terraform is part of Infrastructure as Code, which is when you decleratively make infrastructre out of code, Your infraastructure is code, the rest is abstartcted by our Cloud providers as they provide IaaS (Infrastructure as a Service).

In IAC, you have configuration management and orchestration tools.

**Configuration Managment**
Setting up indivisual servers to perfect desired state.
Or managing thier mutations.

- Chef
- Ansible
- Shell
- Puppet

**Orchestration Managment Tools**
These manages the networking and actual infrastructure (size of machines, subnets, VPC, NACL, SG, IGW, Routes). And deploy instances (machines) into these from AMIs (for example). They can also run small init script (User Data)

- Terraform
- Cloudformation (aws terraform)

## Terraform Objective
The objective of terraform **is to launch Build VPCs, Launch AMIs** (including running init scripts and start services)

**Example Usage**
- Terraform creates VPC
- Terraform sets up SG and NACL abd Subnets
- Terraform deploys AMI's into respective subnets & associated SG to instances 
- Runs some Shell to start services, running init scripts

**Example Usage in DevOps**
- Origin code exists in Bitbucket/GitHub
- Automation gets triggered from chnage in source
- CI pulls code into Jenkins and runs tests
- Passing tests triggers the next job of pipeline (CD)
- Jenkins uses Ansible to change machine to have new code
- Jenkins uses Packer to deliver an AMI
- Jenkins uses Terraform to deploy AMI into production (CD)

**Terraform Main Commands**

Terraform main commands are:
- terraform init
- Terraform plan
- Terraform apply
- Terraform destory


## Terraform Main Sections
- Varialbes 
- Resources 
- Providers

### Rescources aws_instance (ec2) 

Syntax

```terraform

resource "specufuc_resource" "name_in_tf" {
    param_resource = "Value"
    other_param = "Value"
}
```
`specific_resource` is a resource within the library of rhe providers
`name_in_tf` is for calling this resource in tf
`param_resource` are the specific parameters that can be defined for the resource.
For example a ec2 might require an AMI id where a VPC might need a IP range. You need to check your docs.


## Installing Terraform 

## Given Permissions
If running from your machine (thus not inside AWS) - We neeed to give it some AWS secret keys and access keys.

Most clients will manage thier keys differently depending on the level of DevSecOps
sophistication.

### DO NOT 🚨
- Hard code your keys with your code EVER.
- Have them on a side file that is ignored by .gitignore (Still very poor)

### Possible ways of Key Managment

- Set them as enviroment variables fro machine & interpolate (low level of sophistication)
- IAM roles (if you are running in the Cloud)
- Encrypted vaults locally (like Ansible vaults)
- Cloud Vaults (Hashicorp, Ley Managment Services (KMS from AWS))
- 

## Examples and Code

Examples:
- 


## Example ec2 deployment:

```terrafrom

```