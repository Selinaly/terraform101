variable "identify_cohort_name" {
    description = "this is the cohort and name of the person"
    default = "cohort7-selinavar"
}

variable "vpc_id" {
    default = "vpc-4bb64132"
}